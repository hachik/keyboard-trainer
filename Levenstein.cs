﻿namespace LevensteinDistance;

using System;

/// <summary>
/// Методы вычисления дистанции Левенштейна
/// </summary>
public enum dlCalcMethod
{
    cmMatrix,
    cmRecursive,
    cmRecursiveCashed,
    cmDomerau,
}

/// <summary>
/// Вычисляет расстояние между двумя строками (дистанцию Левенштейна)
/// </summary>
public class Levenstein
{
    /// <summary>
    /// Меньшее из нескольких int
    /// </summary>
    /// <param name="values">массив int[]</param>
    /// <returns>Меньшее число</returns>
    private static int Min(params int[] values)
    {
        return values.Min(x => x);
    }

    /// <summary>
    /// Матрица преобразований
    /// </summary>
    private int[,] Matrix
    {
        get
        {
            var m = new int[Len1 + 1, Len2 + 1];

            for (int i = 0; i <= Len1; i++)
                for (int j = 0; j <= Len2; j++)
                {
                    if (i * j == 0)
                        m[i, j] = i + j;
                    else
                        m[i, j] =
                            Min(m[i, j - 1] + 1,
                                m[i - 1, j] + 1,
                                m[i - 1, j - 1] + (Str1?[i - 1] == Str2?[j - 1] ? 0 : 1));
                }

            return m;
        }
    }

    /// <summary>
    /// Получение дистанции Левенштейна с использованием матрицы
    /// </summary>
    private int GetDistance_Matrix()
    {
        if (Len1 + Len2 == 0)
            return Len1 + Len2;

        (int rc, int cc) = (Len1 + 1, Len2 + 1);
        (var prev_row, var cur_row) = (new int[cc], new int[cc]);

        for (int r = 0; r < rc; r++)
        {
            for (int c = 0; c < cc; c++)
            {
                if (r * c == 0)
                    cur_row[c] = r + c;
                else
                    cur_row[c] = Min(prev_row[c] + 1,
                                     cur_row[c - 1] + 1,
                                     prev_row[c - 1] + (Str1[r - 1] == Str2[c - 1] ? 0 : 1));
            }

            Array.Copy(cur_row, prev_row, cc);
        }
        return cur_row[cc - 1];
    }

    /// <summary>
    /// Получение дистанции Дамерау-Левенштейна
    /// </summary>
    /// <remarks>Считает перестановку соседних символов за одну операцию</remarks>
    private int GetDistance_Damerau()
    {
        var m = new int[Len1 + 1, Len2 + 1];

        for (int i = 0; i <= Len1; i++)
            for (int j = 0; j <= Len2; j++)
            {
                if (i * j == 0)
                    m[i, j] = i + j;
                else if (i > 1 && j > 1 && Str1[i - 2] == Str2[j - 1] && Str1[i - 1] == Str2[j - 2])
                    m[i, j] =
                        Min(m[i, j - 1] + 1,
                            m[i - 1, j] + 1,
                            m[i - 1, j - 1] + (Str1?[i - 1] == Str2?[j - 1] ? 0 : 1),
                            m[i - 2, j - 2] + 1);
                else
                    m[i, j] =
                        Min(m[i, j - 1] + 1,
                            m[i - 1, j] + 1,
                            m[i - 1, j - 1] + (Str1?[i - 1] == Str2?[j - 1] ? 0 : 1));
            }

        return m[Len1, Len2];
    }

    /// <summary>
    /// Получение дистанции Левенштейна с использованием рекурсии
    /// <remarks>Если не используется кеш, то затратный метод</remarks>
    private int GetDistance_Recursive(int p1, int p2, int[,]? cash)
    {
        if (p1 * p2 == 0)
            return p1 + p2;

        if ((cash != null) && (cash[p1 - 1, p2 - 1] != -1))
            return cash[p1 - 1, p2 - 1];
        else
        {
            int result = Min(GetDistance_Recursive(p1 - 1, p2, cash) + 1,
                             GetDistance_Recursive(p1, p2 - 1, cash) + 1,
                             GetDistance_Recursive(p1 - 1, p2 - 1, cash) + (Str1[p1 - 1] == Str2[p2 - 1] ? 0 : 1));

            if (cash != null)
                cash[p1 - 1, p2 - 1] = result;

            return result;
        }
    }

    public Levenstein(string Str1, string Str2)
    {
        this.Str1 = Str1;
        this.Str2 = Str2;
    }

    public Levenstein() : this(string.Empty, string.Empty) {; }

    /// <summary>
    /// Строка 1
    /// </summary>
    public string Str1 { get; set; }

    /// <summary>
    /// Строка 2
    /// </summary>
    public string Str2 { get; set; }

    /// <summary>
    /// Длина строки 1
    /// </summary>
    public int Len1 => Str1?.Length ?? 0;

    /// <summary>
    /// Длина строки 2
    /// </summary>
    public int Len2 => Str2?.Length ?? 0;

    /// <summary>
    /// Путь преобразования строки 1 в строку 2
    /// </summary>
    /// <remarks>
    /// D - удалить символ
    /// I - вставитьсимвол
    /// R - заменить символ
    /// M - символ верный
    /// </remarks>
    public char[] Path
    {
        get
        {
            var m = Matrix;
            (int r, int c) = (Str1?.Length ?? 0, Str2?.Length ?? 0);

            List<char> o = new List<char>();

            int d, i, rm;

            while (r + c > 0)
            {
                if (c == 0)
                {
                    o.Add('D');
                    r--;
                }
                else if (r == 0)
                {
                    o.Add('I');
                    c--;
                }
                else
                {
                    rm = m[r - 1, c - 1];
                    i = m[r, c - 1];
                    d = m[r - 1, c];

                    if (rm <= Math.Min(i, d))
                    {
                        if (rm < m[r, c])
                            o.Add('R');
                        else
                            o.Add('M');

                        c--;
                        r--;
                    }
                    else if (i <= d)
                    {
                        o.Add('I');
                        c--;
                    }
                    else
                    {
                        o.Add('D');
                        r--;
                    }
                }
            }

            o.Reverse();
            return o.ToArray();
        }
    }

    /// <summary>
    /// Получить дистанцию
    /// </summary>
    public int GetDistance(dlCalcMethod method)
    {
        if (Len1 * Len2 == 0)
            return Len1 + Len2;

        int[,] InitCache(int m, int n)
        {
            var cache = new int[m, n];
            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    cache[i, j] = -1;

            return cache;
        }

        return method switch
        {
            dlCalcMethod.cmMatrix => GetDistance_Matrix(),
            dlCalcMethod.cmRecursive => GetDistance_Recursive(Len1, Len2, null),
            dlCalcMethod.cmRecursiveCashed => GetDistance_Recursive(Len1, Len2, InitCache(Len1, Len2)),
            dlCalcMethod.cmDomerau => GetDistance_Damerau(),
            _ => -1
        };
    }

    /// <summary>
    /// Дистанция
    /// </summary>
    /// <remarks>
    /// Используется метод матриц
    /// Если нужен другой метод - используйте функцию GetDistance <see cref="GetDistance(CalcMethod)"/>
    /// </remarks>
    public int Distance { get { return GetDistance(dlCalcMethod.cmMatrix); } }
}