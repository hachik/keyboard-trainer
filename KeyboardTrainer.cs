﻿using LevensteinDistance;
using System.Reflection;

namespace KTrainer;

/// <summary>
/// Ражим работы с опечатками
/// </summary>
public enum ktErrorMode
{
    /// <summary>
    /// Блокировать неверный ввод
    /// </summary>
    emBlock,

    /// <summary>
    /// Разрешать неверный ввод
    /// </summary>
    emAllow
}

public class KeyboardTrainer
{
    /// <summary>
    /// Список текстов для теста
    /// </summary>
    private Dictionary<string, List<string>> Texts = new();

    private string _language = null!;

    /// <summary>
    /// Генератор, используемой для получения случайного текста
    /// </summary>
    private Random rnd = new();

    /// <summary>
    /// Используется при получении случайного текста
    /// </summary>
    private int _activeTextIndex = 0;

    /// <summary>
    /// Загружает список текстов из файла Texts.txt
    /// </summary>
    /// <remarks>Тексты долджны быть разделены строкой с 10 символами * (звездочка)</remarks>
    private void LoadTexts()
    {
        foreach (string file in Directory.EnumerateFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)! + "\\text", "*.txt", SearchOption.TopDirectoryOnly))
        {
            Texts.Add(Path.GetFileNameWithoutExtension(file),
                File.ReadAllText(file).Split("**********")
                .Select(t => t.Trim().Replace("\r\n", "\n")).ToList());
        }
    }

    /// <summary>
    /// "Красиво" выводит текст для теста в консоль
    /// </summary>
    private void PrintText()
    {
        SetColor(ConsoleColor.White);
        var MaxStrLen = Math.Min(Text.Split('\r').Max(s => s.Length), Console.WindowWidth);
        Console.WriteLine(new string('-', MaxStrLen));
        Console.WriteLine(Text);
        Console.WriteLine(new string('-', MaxStrLen));
        Console.WriteLine();
    }

    /// <summary>
    /// Печатает результат теста в консоль
    /// </summary>
    private void PrintResults()
    {
        TrainResult result = Statistics[Statistics.Count - 1];

        string msg = string.Format("\n\nВаш результат: {0} символов в минуту", result.PrintRate);

        if (result.ErrorMode == ktErrorMode.emAllow)
            msg += string.Format("; Кол-во ошибок: {0}", result.ErrorsCount);

        if (Statistics.Count() > 1)
            msg += string.Format("\n\nКол-во попыток: {0}\n  лучшая - {2} симв/мин,\n  худшая {3} симв/мин, \n  средняя - {1} симв/мин,",
                Statistics.Count(),
                Math.Floor(Statistics.Average(s => s.PrintRate)),
                Statistics.Max(s => s.PrintRate),
                Statistics.Min(s => s.PrintRate));

        SetColor(ConsoleColor.Gray);

        Console.WriteLine(msg);
    }

    /// <summary>
    /// Подсвечивает ошибки в набранном тексте
    /// </summary>
    private void MarkErrors(string PrintedText)
    {
        var p = (new Levenstein(PrintedText, Text)).Path;

        int InsertionCount = 0;

        Console.SetCursorPosition(0, Console.GetCursorPosition().Top - PrintedText.LinesCount());

        for (int i = 0; i < p.Length; i++)
        {
            switch (p[i])
            {
                case 'M':
                    SetColor(ConsoleColor.Green);
                    Console.Write(PrintedText[i - InsertionCount]);
                    break;
                case 'I':
                    SetColor(ConsoleColor.Red);
                    InsertionCount++;
                    Console.Write('_');
                    break;
                default:
                    SetColor(ConsoleColor.Red);
                    Console.Write(PrintedText[i - InsertionCount]);
                    break;
            }
        }
    }

    /// <summary>
    /// Запрос повтора теста или выхода
    /// </summary>
    private bool ConfirmContinue()
    {
        SetColor(ConsoleColor.Gray);
        Console.WriteLine("\nНажмите Enter, чтобы продолжить\nили Esc, чтобы завершить упражнение\n");

        ConsoleKey key;
        do key = Console.ReadKey(true).Key;
        while (key != ConsoleKey.Enter && key != ConsoleKey.Escape);

        return key == ConsoleKey.Enter;
    }

    /// <summary>
    /// Установка цвета шрифта консоли
    /// </summary>
    private void SetColor(ConsoleColor Color)
    {
        Console.ForegroundColor = Color;
    }

    /// <summary>
    /// Режим работы с ошибками
    /// </summary>
    public ktErrorMode ErrorMode { get; private set; }



    /// <summary>
    /// Конструтктор
    /// </summary>
    /// <param name="ErrorMode">Режим работы с ошибками</param>
    public KeyboardTrainer(ktErrorMode ErrorMode = ktErrorMode.emAllow)
    {
        this.ErrorMode = ErrorMode;
        LoadTexts();
        Statistics = new();
    }

    /// <summary>
    /// Текущий язык
    /// </summary>
    public string Language
    {
        get { return _language; }
        set
        {
            if (!Texts.ContainsKey(value))
                throw new FileNotFoundException($"Файл {value}.txt не найлен");
            else
                _language = value;
        }
    }

    /// <summary>
    /// Выбор языка
    /// </summary>
    public void ChooseLanguage()
    {
        SetColor(ConsoleColor.Gray);
        Console.WriteLine("Выберите язык:");
        for (int i = 0; i < Texts.Keys.Count; i++)
            Console.WriteLine($"{i + 1}: {Texts.Keys.ElementAt(i)}");

        int langIndex;
        while (!int.TryParse(Console.ReadKey(true).KeyChar.ToString(), out langIndex) || (langIndex < 1) || (langIndex > Texts.Keys.Count))
            ;
        Language = Texts.Keys.ElementAt(langIndex - 1);
        Console.WriteLine($"Выбраный язык: {Language}\n");
    }

    /// <summary>
    /// Возвращает текущий текст теста
    /// </summary>
    public string Text => Texts[Language][ActiveTextIndex ?? _activeTextIndex];

    /// <summary>
    /// Индекс текущего текста
    /// </summary>
    /// <remarks>
    /// Устанвите значение в null для использования случайного текста в тесте
    /// </remarks>
    public int? ActiveTextIndex { get; set; }

    public List<TrainResult> Statistics { get; private set; }

    /// <summary>
    /// Запустить процесс тренинга
    /// </summary>
    public void Train(string? Language = null, int? ActiveTextIndex = null)
    {
        this.ActiveTextIndex = ActiveTextIndex;
        SetColor(ConsoleColor.Gray);

        if (Language != null)
            this.Language = Language;
        else if (Texts.Keys.Count == 1)
            this.Language = Texts.Keys.ElementAt(0);
        else if (Texts.Keys.Count > 1)
            ChooseLanguage();
        else
            throw new FileNotFoundException("Файлы с текстами тренинга не найлены");

        Console.WriteLine("Напечатайте предложенный текст.\nНачните печатать - это запустит таймер\n");
        do
        {
            if (ActiveTextIndex == null)
                _activeTextIndex = rnd.Next(0, Texts[Language ?? this.Language].Count);

            PrintText();

            var sl = new List<string>() { Capacity = Text.LinesCount() };

            SetColor(ConsoleColor.Yellow);

            TrainResult result = new();

            if (ErrorMode == ktErrorMode.emAllow)
            {

                char c;
                do c = Console.ReadKey(true).KeyChar;
                while (c != Text[0]);

                Console.Write(c);
                result.StartTime = DateTime.Now; // начнем отсчет времени только после первого правильного символа

                do
                {
                    sl.Add((sl.Count == 0 ? c : "") + Console.ReadLine());
                }
                while (sl.Count() < Text.LinesCount());
            }
            else
            {
                int i = 0;
                do
                {
                    var c = Console.ReadKey(true);

                    if (c.Key == ConsoleKey.Enter && Text[i] == '\n')
                    {
                        i++;
                        Console.WriteLine();
                    }
                    else if (c.KeyChar == Text[i])
                    {
                        if (i == 0)
                            result.StartTime = DateTime.Now;

                        Console.Write(Text[i]);

                        i++;
                    }
                    else
                        Beeper.Beep();
                }
                while (i < Text.Length);
            }

            result.EndTime = DateTime.Now;
            result.CharCount = Text.Length;
            result.ErrorMode = ErrorMode;
            result.ErrorsCount = ErrorMode == ktErrorMode.emBlock ? 0
                : (new Levenstein(string.Join("\n", sl), Text)).GetDistance(dlCalcMethod.cmDomerau);

            Statistics.Add(result);

            if (ErrorMode == ktErrorMode.emAllow)
                MarkErrors(string.Join('\n', sl));

            PrintResults();
        }
        while (ConfirmContinue());
    }
}
