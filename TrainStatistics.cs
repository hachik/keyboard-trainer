﻿namespace KTrainer;
public class TrainResult
{
    public ktErrorMode ErrorMode { get; set; }

    /// <summary>
    /// Время начала теста
    /// </summary>
    public DateTime StartTime { get; set; }

    /// <summary>
    /// Время окончания теста
    /// </summary>
    public DateTime EndTime { get; set; }

    /// <summary>
    /// Количество символов
    /// </summary>
    public int CharCount { get; set; }

    /// <summary>
    /// Количество ошибок
    /// </summary>
    public int ErrorsCount { get; set; }

    /// <summary>
    /// Время печати теста в секундах
    /// </summary>
    public double PrintTime => (EndTime - StartTime).TotalSeconds;

    /// <summary>
    /// Темп печати
    /// </summary>
    /// <remarks>Кол-во символов в минуту</remarks>
    public double PrintRate => CharCount == 0 ? 0 : Math.Floor(60 / (PrintTime / CharCount));
}
