﻿using System.ComponentModel;

namespace KTrainer;

internal class Program
{
    static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.Gray;
        Console.WriteLine("КЛАВИАТУРНЫЙ ТРЕНАЖЕР\n");

        var Language = GetParameter<string?>(ref args, "lang");
        if (string.IsNullOrEmpty(Language))
            Language = null;
        var TextIndex = GetParameter<int?>(ref args, "text");
        var ErrorMode = (ktErrorMode)(GetParameter<int?>(ref args, "mode") ?? 1);

        var kt = new KeyboardTrainer(ErrorMode);
        kt.Train(Language, TextIndex);
    }

    public static T? GetParameter<T>(ref string[] args, string Name)
    {
        try
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T?)converter?.ConvertFromString(args
                .Where(x => x.StartsWith($"-{Name}:"))
                .SingleOrDefault()?
                .Split(':')[1] ?? "") ?? default;
        }
        catch
        {
            return default;
        }
    }
}