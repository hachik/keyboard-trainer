﻿namespace KTrainer;

/// <summary>
/// Ассинхронный бип
/// </summary>
internal class Beeper
{
    static Thread _beepThread;
    static AutoResetEvent _signalBeep;
    static DateTime lastBeep;

    static Beeper()
    {
        lastBeep = DateTime.Now;

        _signalBeep = new AutoResetEvent(false);

        _beepThread = new Thread(() =>
        {
            for (; ; )
            {
                _signalBeep.WaitOne();
                Console.Beep();
            }
        }, 1);
        _beepThread.IsBackground = true;
        _beepThread.Start();
    }

    public static void Beep()
    {
        if ((DateTime.Now - lastBeep).TotalMilliseconds > 400)
            _signalBeep.Set();

        lastBeep = DateTime.Now;
    }
}
