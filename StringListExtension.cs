﻿namespace KTrainer
{
    public static class StringExtension
    {
        public static int LinesCount(this string str)
        {
            return str.Split('\n').Count();
        }
    }
}
